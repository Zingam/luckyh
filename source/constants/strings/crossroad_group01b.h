#ifndef CROSSROAD_GROUP01B_H
#define CROSSROAD_GROUP01B_H

static const char* stringsCrossroad_Group01B[] = {
    "Prepare yourself and choose wisely.",
    "It\'s up to you which way to choose.",
    "Rely on your feelings and you can\'t go wrong.",
    "Do not reach out for help, you are able to solve your current problems on your own."
    "Accept the challenge, you will benefit greatly doing that.",
    "You will need to ask someone for advice.",
    "Try to change your life habits and lower your stress.",
    "Find other ways to solve the problem.",
    "You\'ll have to figure out a way to redeem yourself.",
    "Stop sitting around and waste your life."
};

#endif // CROSSROAD_GROUP01B_H
