#ifndef MISTAKES_GROUP01A_H
#define MISTAKES_GROUP01A_H

static const char* stringsMistakes_Group01A[] = {
    "Today is good day for breaking with bad habits.",
    "Today is good day to relieve yourself of burdens from the past.",
    "You can start over with your life today.",
    "You have been a way too long on the wrong track.",
    "It\'s a great day to start over.",
    "Closing doors from your past will help you evolve."
    "Don\'t feel tempted not to correct a mistake.",
    "Today is the day to take a risk and take care of something that went wrong.",
    "The first steps are always the most difficult ones.",
    "Breaking with bad habits is much easier today than tomorrow." 
};

#endif // MISTAKES_GROUP01A_H
