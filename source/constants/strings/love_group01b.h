#ifndef LOVE_GROUP01B_H
#define LOVE_GROUP01B_H

static const char* stringsLove_Group01B[] = {
    "Embrace the opportunity.",
    "Make the effort, it will certainly be well worth it.",
    "Be honest and you will be successful.",
    "It is high time for you to get in the game.",
    "Stay focused and you will be rewarded.",
    "But be careful, it can also lead to confusion or misunderstandings.",
    "Choose your words carefully, and express them gently.",
    "Do something positive and take a productive line of action.",
    "You could have a great first date. Go for it!",
    "Once you feel that instant rapport, things could progress quickly from here."
};

#endif // LOVE_GROUP01B_H
