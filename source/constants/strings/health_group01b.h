#ifndef HEALTH_GROUP01B_H
#define HEALTH_GROUP01B_H

static const char* stringsHealth_Group01B[] = {
    "Take a break and spend some time in the nature.",
    "Take a day off and spend some time with a good friend.",
    "Don\'t hesitate for asking help and advice. You will be helped.",
    "Don\'t bother to speak about that.",
    "Be more confident, you will be fine.",
    "Do not underestimate the situation.",
    "Even if you work hard you won\'t make much progress today .", 
    "Don\'t get upset, things will change to better soon.",    
    "Accept it if you can\'t change it.",
    "However, don\'t waste time listening to critics."
};

#endif // HEALTH_GROUP01B_H
