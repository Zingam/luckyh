#ifndef LOVE_GROUP01A_H
#define LOVE_GROUP01A_H

static const char* stringsLove_Group01A[] = {
    "High possibilities for interesting encounters this week.",
    "The current astral energy favours your romantic life this week.",
    "There is a powerful indication that your chances are quite good today.",
    "Someone is attracted by you.",
    "You may need to look no farther.",
    "The current planetary alignment has strong postive impact on you.",
    "Someone is interested in you but you do not realize it.",
    "Someone is feeling the same way about you for some time.",
    "You have an opportunity to get in touch with someone who has inspired you to fall in love.",
    "The current aspect at play makes the outlook on your love life that much more exciting."
};

#endif // LOVE_GROUP01A_H
