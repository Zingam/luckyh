#ifndef HELP_GROUP01A_H
#define HELP_GROUP01A_H

static const char* stringsHelp_Group01A[] = {
    "You will get support from unknown source.",
    "You will get a new strong ally.",
    "Stop worrying, help is on the way.",
    "Powerful ally will give a helping hand."
    "The closed doors will soon open before you.",
    "The current planetary alignment implies that you will get full support.",
    "Golden opportunity will be presented to you.",
    "It is impossible to ignore that someone is helping you.",
    "Fate is generous to you.",
    "The stars are revealing good things for you this week." 
};

#endif // HELP_GROUP01A_H
