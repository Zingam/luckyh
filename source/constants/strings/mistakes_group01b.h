#ifndef MISTAKES_GROUP01B_H
#define MISTAKES_GROUP01B_H

static const char* stringsMistakes_Group01B[] = {
    "You will need help in order to make things right.",
    "You will feel like so much better after you changed it to good.",
    "You will have to change from the ground up.",
    "You need to change things if you want to become successful.",
    "It\'s high time to get it done.",
    "But better later that never.",
    "Try to focus on what has to be done before you can get there.",
    "Don\'t underestimate it, it won\'t be an easy task.",
    "It won\'t be easy but it will be worth the struggle.",
    "You will have hard time doing that but you won\'t regret it."    
};

#endif // MISTAKES_GROUP01B_H
