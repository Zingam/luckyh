#ifndef HELP_GROUP01B_H
#define HELP_GROUP01B_H

static const char* stringsHelp_Group01B[] = {
    "Make sure you do the best of it.",
    "But do no get too excited, it is not over yet.",
    "But you have still a lot of work to do.",
    "This is good for you, so keep on.",
    "The time is right to take action.",
    "You\'ll get closer to your goal today.",
    "The good results won\'t be late.",
    "But you still cannot stick to your day schedule.",
    "With the right combination of intuition and hard work you will advance quickly.",
    "However, don\'t hold your breath waiting for fast changes."
};

#endif // HELP_GROUP01B_H
