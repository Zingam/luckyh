#ifndef FINANCE_GROUP01B_H
#define FINANCE_GROUP01B_H

static const char* stringsFinance_Group01B[] = {
    "A good time to make important decisions.",
    "Prepare yourself to win and you will be satisfied with the results.",
    "Your hard work will be rewarded, you will be surprised by the outcomings.",
    "Do not be afraid to advance your opinion.",
    "Today is good idea to face a new challenge.",
    "If you\'ve been thinking of entering a business partnership, now it is the right time.",
    "Unfortunately this is not enough, you will also have to do some convincing.",
    "But put more creativity into your work.",
    "Your success at work rests on your ability to approach people.",
    "But resist temptation, this is simply not the right time."    
};

#endif // FINANCE_GROUP01B_H
