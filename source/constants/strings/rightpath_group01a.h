#ifndef RIGHTPATH_GROUP01A_H
#define RIGHTPATH_GROUP01A_H

static const char* stringsRightpath_Group01A[] = {
    "You are heading in the right direction.",
    "You are on the right path.",
    "There will be some bumbs on the road but you are on your way.",
    "Your failures show you which way to take.",
    "The odds are good that you are on the right track.",
    "You are going to have a breakthrough soon.",
    "The odds are good that today will be a great day for you.", 
    "Fortune smiles upon you today.",
    "It seems that fortune likes you a lot.",
    "You need to take a few more steps in the that direction."
};

#endif // RIGHTPATH_GROUP01A_H
