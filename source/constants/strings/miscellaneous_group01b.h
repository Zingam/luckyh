#ifndef MISCELLANEOUS_GROUP01B_H
#define MISCELLANEOUS_GROUP01B_H

static const char* stringsMiscellaneous_Group01B[] = {
    "Listen carefully if someone offers you an advice.",
    "Do what must be done. Delaying decisions cannot be a solution in a long term.",
    "Lower your expectations. Be thankful for what you get.",
    "Postpone travel and educational plans.",
    "Do no hesitate to correct a mistake.",
    "Avoid judging about others. This will distract you from your goals.",
    "Give your advice only if you asked to do that.",
    "Relations with colleagues will suffer.",
    "Do not get tempted to express your opinion if not asked to do it.",
    "Figure this out first and then handle."
};

#endif // MISCELLANEOUS_GROUP01B_H
