#ifndef LESSMONEY_GROUP01B_H
#define LESSMONEY_GROUP01B_H

static const char* stringsLessmoney_Group01B[] = {
    "You have to work harder.",
    "You have to bundle resources in order to handle the situation.",
    "It will be necessary to cut your expences.",
    "Ask for a short term loan.",
    "Your good mood matters more than ever today.",
    "Make sure you choose the right words if you need support.",
    "The right answer to the problem is the answer that is obvious.",
    "Help yourself and get ahead.",
    "Consider it a necessity that changes are imminent.",
    "You have to come up with plenty of ways to cut costs."
};

#endif // LESSMONEY_GROUP01B_H
