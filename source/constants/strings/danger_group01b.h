#ifndef DANGER_GROUP01B_H
#define DANGER_GROUP01B_H

static const char* stringsDanger_Group01B[] = {
    "Do not hesitate to look for help.",
    "Prepare good so you can make sure you can handle it.",
    "Do not underestimate, handle with caution.",
    "Look for an opportunity to help yourself.",
    "Keep your ears open and act wisely.",
    "If you are accused of being too emotional today, don\'t take it too personally.",
    "Try to stick with the plan today.",
    "Make sure to keep an open mind.",
    "Spend some time socializing with colleagues, that is certainly a good idea now.",  
    "No need to get frustrated, just try not to make some silly statements."
};

#endif // DANGER_GROUP01B_H
