#ifndef RIGHTPATH_GROUP01B_H
#define RIGHTPATH_GROUP01B_H

static const char* stringsRightpath_Group01B[] = {
    "Be consistent and your hard work will be awarded.",
    "But you have to carry on going.",
    "But you still have a long way to walk.",
    "Don\'t be a fool, you need to work hard as anybody else.",
    "But don\'t assume that you are destined to have success.",
    "Don\'t get tempted to stop now.",
    "Expand your horizons and embrace this new way of life.",
    "Show everyone what you are capable of.",
    "You\'ll have to figure out what comes next.",
    "Nevertheless you will need all the help you can get at the moment."
};

#endif // RIGHTPATH_GROUP01B_H
