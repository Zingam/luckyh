#ifndef LESSMONEY_GROUP01A_H
#define LESSMONEY_GROUP01A_H

static const char* stringsLessmoney_Group01A[] = {
    "Your ability to meet financial obligations will decrease in near future.",
    "Unexpected happenings will make your situation worse.",
    "You are dogded by bad luck.",
    "You will get some bad news.",
    "You can expect some serious troubles soon.",
    "Venus is influencing you in a bad way.",
    "The wrong reasons just cannot be a good advisor.",
    "Your intuitive powers will let you down today.",
    "Bad energies are distracting you.",
    "Unfaithful intentions always result in disaster."
};

#endif // LESSMONEY_GROUP01A_H
