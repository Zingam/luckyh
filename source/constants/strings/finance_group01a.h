#ifndef FINANCE_GROUP01A_H
#define FINANCE_GROUP01A_H

static const char* stringsFinance_Group01A[] = {
    "The planets favour your business activities this week.",
    "The presence of four planets is a powerful motivating force for you right now.",
    "You will benefit greatly from the position of two planets this week.",
    "The stars are on your side this week.",
    "You will get the full support of your colleagues.",
    "Fortune smiles upon you today.",
    "The odds are good for you this weekend.",
    "Today\'s planetary energies are entirely on your side.",
    "Your financial situation at the moment offers you plenty of possibilities.",
    "You have some brilliant ideas today."   
};

#endif // FINANCE_GROUP01A_H
