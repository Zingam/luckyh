#ifndef DANGER_GROUP01A_H
#define DANGER_GROUP01A_H

static const char* stringsDanger_Group01A[] = {
    "Disadvantageous happening is upon you.",
    "Your emotions are not helping you today.",
    "Any change in your routine will cause troubles.",
    "Current astral energies indicate that you are on the wrong track.",
    "The odds are bad for you today.",
    "You will face some serious resistance.", 
    "You won\'t be able to handle today's challenges.",
    "Tension with a friend will give you some headache.",
    "You will feel deceived today.",
    "Most likely it will not turn out the way you imagine it to."   
};

#endif // DANGER_GROUP01A_H
