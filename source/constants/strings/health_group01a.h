#ifndef HEALTH_GROUP01A_H
#define HEALTH_GROUP01A_H

static const char* stringsHealth_Group01A[] = {
    "You are having a hard time right now.",
    "Mercury has strong negative impact on you this month, your creativity will suffer.",
    "Mars demotivates you and you feel lack of energy this month.",
    "Your emotional needs are kind of complex right now.",
    "Disadvantageous star constellation will make you less efficient.",
    "You are feeling not well today.",
    "Today is simply not your day.",
    "You wont\'t be able to meet your goals today.",
    "Bad impact on you will slow you down.",
    "Your intuition is deceiving you today."
};

#endif // HEALTH_GROUP01A_H
