#ifndef CROSSROAD_GROUP01A_H
#define CROSSROAD_GROUP01A_H

static const char* stringsCrossroad_Group01A[] = {
    "You are on crossroad.",
    "Difficult decisions lie in front of you.",
    "You will be confronted with serious challenges this month.",
    "This week will be challenging.",
    "Obstacles will slow you down.",
    "Your week is off to a not especially good start.",
    "You\'ll stumble onto some problems.",
    "You will have a hard time keeping yourself grounded.",
    "Your head is revolving in a whirl of confusion.",
    "Lack of communication will cause some serious troubles."    
};

#endif // CROSSROAD_GROUP01A_H
