#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cstddef>

#include "strings/__test_group01a.h"
#include "strings/__test_group01b.h"
#include "strings/crossroad_group01a.h"
#include "strings/crossroad_group01b.h"
#include "strings/danger_group01a.h"
#include "strings/danger_group01b.h"
#include "strings/finance_group01a.h"
#include "strings/finance_group01b.h"
#include "strings/health_group01a.h"
#include "strings/health_group01b.h"
#include "strings/help_group01a.h"
#include "strings/help_group01b.h"
#include "strings/lessmoney_group01a.h"
#include "strings/lessmoney_group01b.h"
#include "strings/love_group01a.h"
#include "strings/love_group01b.h"
#include "strings/miscellaneous_group01a.h"
#include "strings/miscellaneous_group01b.h"
#include "strings/mistakes_group01a.h"
#include "strings/mistakes_group01b.h"
#include "strings/rightpath_group01a.h"
#include "strings/rightpath_group01b.h"

enum ZodiacSign
{
    Aries,
    Taurus,
    Gemini,
    Cancer,
    Leo,
    Virgo,
    Libra,
    Scorpio,
    Sagittarius,
    Capricornus,
    Aquarius,
    Pisces
};

// Returns the size of an array
template <typename ARRAY,size_t arraySize>
inline size_t getArraySize(const ARRAY (&a)[arraySize])
{
    return arraySize;
}

#endif