#ifndef GENERATOR_H
#define GENERATOR_H

#include <vector>

#include "constants/Constants.h"
#include "PredictionCategory.h"

class Generator
{
public:
    Generator(void);
    ~Generator(void);

    const char* makePrediction(ZodiacSign zodiacSign);
private:
    int stringsLength;
    
    std::vector<PredictionCategory> data;

    void initializeData(void);

    const char* compilePrediction(ZodiacSign zodiacSign, int primeNumber);
};

#endif // GENERATOR_H
