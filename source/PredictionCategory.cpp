#include "PredictionCategory.h"

#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <iostream>

#include "constants/Constants.h"
#include "utils/rngs.h"


PredictionCategory::PredictionCategory()
{
}

PredictionCategory::PredictionCategory(const char **stringsArray01, size_t stringsArray01_Size, 
                                       const char **stringsArray02, size_t stringsArray02_Size)
{
    this->stringsArray01 = stringsArray01;
    this->stringsArray01_Size = stringsArray01_Size;
    
    this->stringsArray02 = stringsArray02;
    this->stringsArray02_Size = stringsArray02_Size;
}


PredictionCategory::~PredictionCategory(void)
{
    //std::cout << "Before free" << std::endl;
    //free(stringPrediction);
    //std::cout << "After free" << std::endl;
}

const char* PredictionCategory::makePrediction(ZodiacSign zodiacSign)
{
    //std::cout << "Begin prediction" << std::endl;
    time_t currentRawTime = time(NULL);
    struct tm *currentTime;
    currentTime = localtime(&currentRawTime);

    srand(currentTime->tm_mday + zodiacSign);

    size_t index01 = rand() % stringsArray01_Size;
    std::cout << " String01: " << index01; 
    const char *string01 = stringsArray01[index01];
    size_t string01_Size = strlen(string01);

    size_t index02 = rand() % stringsArray02_Size;
    std::cout << " String02: " << index02; 
    const char *string02 = stringsArray02[index02];
    size_t string02_Size = strlen(string02);

    static char stringPrediction[2000];

    //std::cout << "Before malloc" << std::endl;
    //stringPrediction = (char *)malloc(string01_Size + 1 + string02_Size + 1);
    //std::cout << "After malloc" << std::endl;
    
    strcpy(stringPrediction, string01);
    //std::cout << "string 1..." << std::endl;
    strcat(stringPrediction, " ");
    //std::cout << "space" << std::endl;
    strcat(stringPrediction, string02);
    //std::cout << "string 2..." << std::endl;

    //std::cout << "             STARTING" << std::endl;
    //std::cout << "string01 = " << string01 << std::endl;
    //std::cout << "string01_Size = " << string01_Size << std::endl;
    //std::cout << "string02 = " << string02 << std::endl;
    //std::cout << "string02_Size = " << string02_Size << std::endl;
    //std::cout << "stringPrediction = " << stringPrediction << std::endl;

    //std::cout << "Returning" << std::endl;
    return stringPrediction;
}
