#ifndef PREDICTIONCATEGORY_H
#define PREDICTIONCATEGORY_H

#include "constants/Constants.h"

class PredictionCategory
{
public:
    PredictionCategory();
    PredictionCategory(const char **stringsArray01, size_t stringsArray01_Size, 
                       const char **stringsArray02, size_t stringsArray02_Size);
    ~PredictionCategory(void);

    const char* makePrediction(ZodiacSign zodiacSign);
private:
    char *stringPrediction;

    const char **stringsArray01;
    int stringsArray01_Size;

    const char **stringsArray02;
    int stringsArray02_Size;
};

#endif // PREDICTIONCATEGORY_H
