#ifndef GLOBALS_H
#define GLOBALS_H

#include "IwUIElement.h"
#include "Generator.h"

static CIwUIElement *gScreen;

static CIwUIElement *pScreenSelectZodiac;
static CIwUIElement *pScreenShowZodiac;

static Generator *generator;

#endif // GLOBALS_H
