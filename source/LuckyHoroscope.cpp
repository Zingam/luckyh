/********************************************************************************
 *
 * L.U.C.K.Y.H.O.R.O.S.C.O.P.E
 *
 ********************************************************************************/

//#define EXTERNAL_CCONTROLLER

#include "s3eDevice.h"
#include "IwUI.h"

#ifdef EXTERNAL_CCONTROLLER
#include "CController.h"
#endif EXTERNAL_CCONTROLER
#include "Generator.h"
#include "Globals.h"

#ifndef EXTERNAL_CCONTROLLER
class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Aries, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Taurus, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Gemini, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Cancer, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Leo, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Virgo, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Libra, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Scorpio, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Sagittarius, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Capricornus, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Aquarius, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Pisces, CIwUIElement*)

        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Close, CIwUIElement*)
    }

private:
    const char* getZodiacText(ZodiacSign zodiacSign)
    {
        switch (zodiacSign)
        {
        case Aries: return "gAries";
        case Taurus: return "Taurus";
        case Gemini: return "Gemini";
        case Cancer: return "Cancer";
        case Leo: return "Leo";
        case Virgo: return "Virgo";
        case Libra: return "Libra";
        case Scorpio: return "Scorpio";
        case Sagittarius: return "Sagittarius";
        case Capricornus: return "Capricornus";
        case Aquarius: return "Aquarius";
        case Pisces: return "Pisces";
        }

        return "Error: unknown";
    }

    void SetZodiacScreen(ZodiacSign zodiacSign, CIwSVec2 uv0, CIwSVec2 uv1)
    {
        CIwUIImage* image = IwSafeCast<CIwUIImage*>(pScreenShowZodiac->GetChildNamed("ZodiacImage"));

        image->SetProperty("uv0", uv0);
        image->SetProperty("uv1", uv1);

        CIwUILabel* labelTitleBar = IwSafeCast<CIwUILabel*>(pScreenShowZodiac->GetChildNamed("TitleBarLabel"));

        labelTitleBar->SetCaption(getZodiacText(zodiacSign));

        CIwUILabel* label = IwSafeCast<CIwUILabel*>(pScreenShowZodiac->GetChildNamed("Label_ZodiacText_Caption"));

        Generator generator;
        label->SetCaption(generator.makePrediction(zodiacSign));

        pScreenSelectZodiac->SetVisible(false);
        pScreenShowZodiac->SetVisible(true);
    };

    void OnClick_Aries(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Aries;

        SetZodiacScreen(zodiac, CIwSVec2(632, 2), CIwSVec2(837, 197));
    }

    void OnClick_Taurus(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Taurus;

        SetZodiacScreen(zodiac, CIwSVec2(2, 200), CIwSVec2(208, 395));
    }

    void OnClick_Gemini(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Gemini;

        SetZodiacScreen(zodiac, CIwSVec2(211, 200), CIwSVec2(418, 395));
    }

    void OnClick_Cancer(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Cancer;

        SetZodiacScreen(zodiac, CIwSVec2(421, 200), CIwSVec2(628, 395));
    }

    void OnClick_Leo(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Leo;

        SetZodiacScreen(zodiac, CIwSVec2(632, 200), CIwSVec2(837, 395));
    }

    void OnClick_Virgo(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Virgo;

        SetZodiacScreen(zodiac, CIwSVec2(2, 398), CIwSVec2(208, 590));
    }

    void OnClick_Libra(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Libra;

        SetZodiacScreen(zodiac, CIwSVec2(211, 398), CIwSVec2(418, 590));
    }

    void OnClick_Scorpio(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Scorpio;

        SetZodiacScreen(zodiac, CIwSVec2(421, 398), CIwSVec2(628, 590));
    }

    void OnClick_Sagittarius(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Sagittarius;

        SetZodiacScreen(zodiac, CIwSVec2(631, 398), CIwSVec2(837, 590));
    }

    void OnClick_Capricornus(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Capricornus;

        SetZodiacScreen(zodiac, CIwSVec2(2, 2), CIwSVec2(208, 197));
    }

    void OnClick_Aquarius(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Gemini;

        SetZodiacScreen(zodiac, CIwSVec2(211, 2), CIwSVec2(418, 197));
    }

    void OnClick_Pisces(CIwUIElement* Clicked)
    {
        ZodiacSign zodiac = Pisces;

        SetZodiacScreen(zodiac, CIwSVec2(421, 2),  CIwSVec2(628, 197));
    }

    void OnClick_Close(CIwUIElement* Clicked)
    {
        pScreenSelectZodiac->SetVisible(true);
        pScreenShowZodiac->SetVisible(false);
    }
};
#endif EXTERNAL_CCONTROLLER

int main() 
{
    IwUIInit();
    new CIwUIView();
    new CController();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("LuckyHoroscopeUI.group");

    pScreenSelectZodiac = CIwUIElement::CreateFromResource("ScreenSelectZodiac");
    pScreenShowZodiac = CIwUIElement::CreateFromResource("ScreenShowZodiac");

    generator = new Generator();

    IwGetUIView()->AddElement(pScreenSelectZodiac);
    IwGetUIView()->AddElement(pScreenShowZodiac);

    pScreenSelectZodiac->SetVisible(true);
    pScreenShowZodiac->SetVisible(false);

    IwGetUIView()->AddElementToLayout(pScreenSelectZodiac);
    IwGetUIView()->AddElementToLayout(pScreenShowZodiac);
    
    const int32 frametime = 25;

    while (!s3eDeviceCheckQuitRequest())
    {
        s3eDeviceYield(0);
        s3eKeyboardUpdate();

        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        //Render Debugging outlines of the element hierarchy
        IwUIDebugRender(NULL, IW_UI_DEBUG_LAYOUT_OUTLINE_F | IW_UI_DEBUG_ELEMENT_OUTLINE_F);
        
        IwGetUIController()->Update();
        IwGetUIView()->Update(frametime);
        
        IwGetUIView()->Render();
        
        IwGxFlush();
        IwGxSwapBuffers();
    }

    delete generator;

    delete IwGetUIController();
    delete IwGetUIView();

    IwUITerminate();

    return 0;
}