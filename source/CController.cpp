#include "IwUI.h"
#include "IwUIElement.h"
#include "IwUIController.h"

#include "constants/Constants.h"
#include "CController.h"
#include "Globals.h"


CController::CController()
{
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Aries, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Taurus, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Gemini, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Cancer, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Leo, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Virgo, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Libra, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Scorpio, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Sagittarius, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Capricornus, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Aquarius, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Pisces, CIwUIElement*)

    IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Close, CIwUIElement*)
}

const char* CController::getZodiacText(ZodiacSign zodiacSign)
{
    switch (zodiacSign)
    {
    case Aries: return "Aries";
    case Taurus: return "Taurus";
    case Gemini: return "Gemini";
    case Cancer: return "Cancer";
    case Leo: return "Leo";
    case Virgo: return "Virgo";
    case Libra: return "Libra";
    case Scorpio: return "Scorpio";
    case Sagittarius: return "Sagittarius";
    case Capricornus: return "Capricornus";
    case Aquarius: return "Aquarius";
    case Pisces: return "Pisces";
    }

    return "Error: unknown";
}

void CController::SetZodiacScreen(ZodiacSign zodiacSign, CIwSVec2 uv0, CIwSVec2 uv1)
{
    CIwUIElement *element = pScreenShowZodiac;

    CIwUIImage* image = IwSafeCast<CIwUIImage*>(pScreenShowZodiac->GetChildNamed("ZodiacImage"));

    image->SetProperty("uv0", uv0);
    image->SetProperty("uv1", uv1);

    CIwUILabel* labelTitleBar = IwSafeCast<CIwUILabel*>(pScreenShowZodiac->GetChildNamed("TitleBarLabel"));

    labelTitleBar->SetCaption(getZodiacText(zodiacSign));

    CIwUILabel* label = IwSafeCast<CIwUILabel*>(pScreenShowZodiac->GetChildNamed("Label_ZodiacText_Caption"));
    
    label->SetCaption(generator->makePrediction(zodiacSign));

    pScreenSelectZodiac->SetVisible(false);
    pScreenShowZodiac->SetVisible(true);
};

void CController::OnClick_Aries(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Aries;

    SetZodiacScreen(zodiac, CIwSVec2(632, 2), CIwSVec2(837, 197));
}

void CController::OnClick_Taurus(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Taurus;

    SetZodiacScreen(zodiac, CIwSVec2(2, 200), CIwSVec2(208, 395));
}

void CController::OnClick_Gemini(CIwUIElement* Clicked)
    {
    ZodiacSign zodiac = Gemini;

    SetZodiacScreen(zodiac, CIwSVec2(211, 200), CIwSVec2(418, 395));
}

void CController::OnClick_Cancer(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Cancer;

    SetZodiacScreen(zodiac, CIwSVec2(421, 200), CIwSVec2(628, 395));
}

void CController::OnClick_Leo(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Leo;

    SetZodiacScreen(zodiac, CIwSVec2(632, 200), CIwSVec2(837, 395));
}

void CController::OnClick_Virgo(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Virgo;

    SetZodiacScreen(zodiac, CIwSVec2(2, 398), CIwSVec2(208, 590));
}

void CController::OnClick_Libra(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Libra;

    SetZodiacScreen(zodiac, CIwSVec2(211, 398), CIwSVec2(418, 590));
}

void CController::OnClick_Scorpio(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Scorpio;

    SetZodiacScreen(zodiac, CIwSVec2(421, 398), CIwSVec2(628, 590));
}

void CController::OnClick_Sagittarius(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Sagittarius;

    SetZodiacScreen(zodiac, CIwSVec2(631, 398), CIwSVec2(837, 590));
}

void CController::OnClick_Capricornus(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Capricornus;

    SetZodiacScreen(zodiac, CIwSVec2(2, 2), CIwSVec2(208, 197));
}

void CController::OnClick_Aquarius(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Gemini;

    SetZodiacScreen(zodiac, CIwSVec2(211, 2), CIwSVec2(418, 197));
}

void CController::OnClick_Pisces(CIwUIElement* Clicked)
{
    ZodiacSign zodiac = Pisces;

    SetZodiacScreen(zodiac, CIwSVec2(421, 2),  CIwSVec2(628, 197));
}

void CController::OnClick_Close(CIwUIElement* Clicked)
{
    pScreenSelectZodiac->SetVisible(true);
    pScreenShowZodiac->SetVisible(false);
}
