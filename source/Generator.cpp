#include "Generator.h"

#include <iostream>

#include <stdlib.h>
#include <time.h>

#include "constants/Constants.h"
#include "utils/rngs.h"


Generator::Generator(void)
{
    this->initializeData();
}


Generator::~Generator(void)
{
}

const char* Generator::compilePrediction(ZodiacSign zodiacSign, int primeNumber)
{
    std::cout << "Zodiac: " << zodiacSign << " --> ";

    time_t currentRawTime = time(NULL);
    struct tm *currentTime;
    currentTime = localtime(&currentRawTime);

    PlantSeeds(currentTime->tm_mday + zodiacSign + primeNumber);
    
    // Prediction category 1
    //srand(currentTime->tm_mday + zodiacSign + primeNumber);
    //
    //size_t indexPredictionCategory01 = rand() % this->data.size();

    SelectStream(1);
    size_t indexPredictionCategory01 = (size_t)(Random() * 2147483647) % this->data.size();

    std::cout << " Category 1: " << indexPredictionCategory01; 

    PredictionCategory predictionCategory01 = this->data[indexPredictionCategory01];
    
    const char* string01 = "fghdfgh";//predictionCategory01.makePrediction(zodiacSign);

    // Prediction category 2
    //srand(currentTime->tm_mday + zodiacSign + primeNumber);
    //
    //size_t indexPredictionCategory02 = rand() % this->data.size();

    SelectStream(2);
    size_t indexPredictionCategory02 = ((size_t)(Random() * 2147483647) + 1) % this->data.size();

    std::cout << "; Category 2: " << indexPredictionCategory02;

    PredictionCategory predictionCategory02 = this->data[indexPredictionCategory02];
    
    const char* string02 = predictionCategory02.makePrediction(zodiacSign);

    std::cout << std::endl;

    static char stringPrediction[2000];

    //std::cout << "Before malloc" << std::endl;
    //stringPrediction = (char *)malloc(string01_Size + 1 + string02_Size + 1);
    //std::cout << "After malloc" << std::endl;
    
    strcpy(stringPrediction, string01);
    //std::cout << "string 1..." << std::endl;
    strcat(stringPrediction, " ");
    //std::cout << "space" << std::endl;
    strcat(stringPrediction, string02);
    //std::cout << "string 2..." << std::endl;

    //std::cout << "             STARTING" << std::endl;
    //std::cout << "string01 = " << string01 << std::endl;
    //std::cout << "string01_Size = " << string01_Size << std::endl;
    //std::cout << "string02 = " << string02 << std::endl;
    //std::cout << "string02_Size = " << string02_Size << std::endl;
    //std::cout << "stringPrediction = " << stringPrediction << std::endl;

    //std::cout << "Returning" << std::endl;
    return stringPrediction;
}

const char* Generator::makePrediction(ZodiacSign zodiacSign)
{
    time_t currentRawTime = time(NULL);
    struct tm *currentTime;
    currentTime = localtime(&currentRawTime);

    PredictionCategory predictionCategory;

    size_t index;

    switch (zodiacSign)
    {
    case Aries:
        srand(currentTime->tm_mday + zodiacSign + 1019);
        index = rand() % data.size();
        std::cout << "Aries " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        //return compilePrediction(zodiacSign, 1019);
        break; 
    case Taurus:
        srand(currentTime->tm_mday + zodiacSign + 1381);
        index = rand() % data.size();
        std::cout << "Taurus " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break; 
    case Gemini:
        srand(currentTime->tm_mday + zodiacSign + 1901);
        index = rand() % data.size();
        std::cout << "Gemini " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Cancer:
        srand(currentTime->tm_mday + zodiacSign + 2437);
        index = rand() % data.size();
        std::cout << "Cancer " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Leo:
        srand(currentTime->tm_mday + zodiacSign + 3001);
        index = rand() % data.size();
        std::cout << "Leo " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Virgo:
        srand(currentTime->tm_mday + zodiacSign + 3433);
        index = rand() % data.size();
        std::cout << "Virgo " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Libra:
        srand(currentTime->tm_mday + zodiacSign + 4153);
        index = rand() % data.size();
        std::cout << "Libra " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Scorpio:
        srand(currentTime->tm_mday + zodiacSign + 4663);
        index = rand() % data.size();
        std::cout << "Scorpio " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Sagittarius:
        srand(currentTime->tm_mday + zodiacSign + 5039);
        index = rand() % data.size();
        std::cout << "Sagittarius " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Capricornus:
        srand(currentTime->tm_mday + zodiacSign + 5867);
        index = rand() % data.size();
        std::cout << "Capricornus " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Aquarius:
        srand(currentTime->tm_mday + zodiacSign + 6961);
        index = rand() % data.size();
        std::cout << "Aquarius " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    case Pisces:
        srand(currentTime->tm_mday + zodiacSign + 7547);
        index = rand() % data.size();
        std::cout << "Pisces " << index << " " << data.size() << std::endl;
        predictionCategory = this->data[index];
        return predictionCategory.makePrediction(zodiacSign);
        break;
    default: return "Unrecognized Zodiac sign";
    }
}

void Generator::initializeData(void)
{
    std::cout << "Creating \"Crossroad\"" << std::endl;
    PredictionCategory crossroad     = PredictionCategory(stringsCrossroad_Group01A, getArraySize(stringsCrossroad_Group01A),
                                                          stringsCrossroad_Group01B, getArraySize(stringsCrossroad_Group01B));
    this->data.push_back(crossroad);
    std::cout << "Prediction: " << this->data.at(0).makePrediction(Aries) << std::endl;

    std::cout << "Creating \"Danger\"" << std::endl;
    PredictionCategory danger        = PredictionCategory(stringsDanger_Group01A, getArraySize(stringsDanger_Group01A),
                                                          stringsDanger_Group01B, getArraySize(stringsDanger_Group01B));
    this->data.push_back(danger);
    std::cout << "Prediction: " << this->data.at(1).makePrediction(Aries) << std::endl;

    PredictionCategory finance       = PredictionCategory(stringsFinance_Group01A, getArraySize(stringsFinance_Group01A),
                                                          stringsFinance_Group01B, getArraySize(stringsFinance_Group01B));
    this->data.push_back(finance);

    PredictionCategory health   = PredictionCategory(stringsHealth_Group01A, getArraySize(stringsHealth_Group01A),
                                                          stringsHealth_Group01B, getArraySize(stringsHealth_Group01B));
    this->data.push_back(health);

    PredictionCategory help          = PredictionCategory(stringsHelp_Group01A, getArraySize(stringsHelp_Group01A),
                                                          stringsHelp_Group01B, getArraySize(stringsHelp_Group01B));
    this->data.push_back(help);

    PredictionCategory lessmoney     = PredictionCategory(stringsLessmoney_Group01A, getArraySize(stringsLessmoney_Group01A),
                                                          stringsLessmoney_Group01B, getArraySize(stringsLessmoney_Group01B));
    this->data.push_back(lessmoney);

    PredictionCategory love          = PredictionCategory(stringsLove_Group01A, getArraySize(stringsLove_Group01A),
                                                          stringsLove_Group01B, getArraySize(stringsLove_Group01B));
    this->data.push_back(love);

    PredictionCategory miscellaneous = PredictionCategory(stringsMiscellaneous_Group01A, getArraySize(stringsMiscellaneous_Group01A),
                                                          stringsMiscellaneous_Group01B, getArraySize(stringsMiscellaneous_Group01B));
    this->data.push_back(miscellaneous);

    PredictionCategory mistakes      = PredictionCategory(stringsMistakes_Group01A, getArraySize(stringsMistakes_Group01A),
                                                          stringsMistakes_Group01B, getArraySize(stringsMistakes_Group01B));
    this->data.push_back(mistakes);

    PredictionCategory rightpath     = PredictionCategory(stringsRightpath_Group01A, getArraySize(stringsRightpath_Group01A),
                                                          stringsRightpath_Group01B, getArraySize(stringsRightpath_Group01B));
    this->data.push_back(rightpath);
}
