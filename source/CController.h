#ifndef CCONTROLLER_H
#define CCONTROLLER_H

#include "constants/Constants.h"

class CController : public CIwUIController
{
public:
    CController();

private:
    const char* getZodiacText(ZodiacSign zodiacSign);

    void SetZodiacScreen(ZodiacSign zodiacSign, CIwSVec2 uv0, CIwSVec2 uv1);

    void OnClick_Aries(CIwUIElement* Clicked);
    void OnClick_Taurus(CIwUIElement* Clicked);
    void OnClick_Gemini(CIwUIElement* Clicked);
    void OnClick_Cancer(CIwUIElement* Clicked);
    void OnClick_Leo(CIwUIElement* Clicked);
    void OnClick_Virgo(CIwUIElement* Clicked);
    void OnClick_Libra(CIwUIElement* Clicked);
    void OnClick_Scorpio(CIwUIElement* Clicked);
    void OnClick_Sagittarius(CIwUIElement* Clicked);
    void OnClick_Capricornus(CIwUIElement* Clicked);
    void OnClick_Aquarius(CIwUIElement* Clicked);
    void OnClick_Pisces(CIwUIElement* Clicked);
    void OnClick_Close(CIwUIElement* Clicked);
};

#endif // CCONTROLLER_H
